using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace bbDB.Core {
	public class Database {
		public string Path { get; }
		public Encoding Encoding { get; }

		public Database(string path) {
			Path = path;
			Encoding = Encoding.UTF8;
			createDatabase();
		}

		public Database(string path, Encoding encoding) : this(path) {
			Encoding = encoding;
		}

		public Document<T> GetDocument<T>(string name) {
			var document = !documentExists(name) ? new Document<T>(name, typeof(T)) : loadDocument<T>(name);
			document.OnSave += saveDocument;
			return document;
		}

		private Document<T> loadDocument<T>(string name) {
			var path = getDocumentPath(name);
			using var sr = new StreamReader(path, Encoding);
			var dataText = sr.ReadToEnd();
			var data = JsonConvert.DeserializeObject<Document<T>>(dataText);
			return data;
		}

		private void saveDocument<T>(Document<T> document) {
			var path = getDocumentPath(document.Name);
			using var sw = new StreamWriter(path, false, Encoding);
			var dataText = JsonConvert.SerializeObject(document);
			sw.Write(dataText);
		}


		private string getDocumentPath(string name) {
			name = name.ToBase64(Encoding);
			var path = System.IO.Path.Combine(Path, name);
			return path;
		}

		private bool documentExists(string name) {
			var path = getDocumentPath(name);
			return File.Exists(path);
		}

		private bool databaseExists() {
			return Directory.Exists(Path);
		}

		private void createDatabase() {
			if (databaseExists()) {
				return;
			}

			Directory.CreateDirectory(Path);
		}
	}
}