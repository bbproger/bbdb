using System;

namespace bbDB.Core {
	public class DocumentEntry<T> {
		public Guid Id { get; set; }
		public T Data { get; private set; }

		public DocumentEntry(T data) {
			Id = Guid.NewGuid();
			Data = data;
		}

		public void SetData(T data) {
			Data = data;
		}
	}
}