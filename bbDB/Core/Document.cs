using System;
using System.Collections.Generic;
using System.Linq;

namespace bbDB.Core {
	public class Document<T> {
		public string Name { get; set; }
		public Type Type { get; set; }
		public IEnumerable<DocumentEntry<T>> Collection { get; set; }

		public int Count => Collection.Count();

		internal event Action<Document<T>> OnSave;

		public Document(string name, Type type) {
			Name = name;
			Type = type;
			Collection = Enumerable.Empty<DocumentEntry<T>>();
		}

		public void Append(T item) {
			var entry = new DocumentEntry<T>(item);
			Collection = Collection.Append(entry);
			Save();
		}

		public T FirstOrDefault(Func<T, bool> predicate) {
			var entry = Collection.FirstOrDefault(e => predicate.Invoke(e.Data));
			return entry == null ? default : entry.Data;
		}

		public bool ReplaceOne(Func<T, bool> filter, T replacement) {
			var entry = Collection.FirstOrDefault(e => filter.Invoke(e.Data));
			if (entry == null) {
				return false;
			}

			entry.SetData(replacement);
			Save();
			return true;
		}

		public int Remove(Func<T, bool> predicate) {
			var items = Collection.Where(entry => !predicate.Invoke(entry.Data));
			var count = Collection.Count() - items.Count();
			Collection = items;
			Save();
			return count;
		}

		public IEnumerable<T> GetAll() {
			return Collection.Select(entry => entry.Data);
		}

		private void Save() {
			OnSave?.Invoke(this);
		}
	}
}