using System;
using System.Text;

namespace bbDB.Core {
	public static class Extension {
		public static string ToBase64(this string str, Encoding encoding = null) {
			return Convert.ToBase64String((encoding ?? Encoding.UTF8).GetBytes(str));
		}

		public static string FromBase64(this string str, Encoding encoding = null) {
			return (encoding ?? Encoding.UTF8).GetString(Convert.FromBase64String(str));
		}
	}
}