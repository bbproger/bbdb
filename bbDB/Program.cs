﻿using System;
using bbDB.Core;

namespace bbDB {
	public class User {
		public string Id { get; set; }
		public string Name { get; set; }

		public User(string name) {
			Id = Guid.NewGuid().ToString();
			Name = name;
		}

		public override string ToString() {
			return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}";
		}
	}

	class Program {
		static void Main(string[] args) {
			var db = new Database("/Users/armankarapetyan/Desktop/db");
			var document = db.GetDocument<User>(nameof(User));
			document.Append(new User("Arman"));
			document.Append(new User("Gev"));
			// var replace = document.ReplaceOne(user => user.Id.Equals("acfb3303-e2c6-4fdd-bc5f-1095431f9817"),
			// 	new User("Lolo"));
			Console.WriteLine(document.Count);
		}
	}
}